
group = "com.ichromanrd.sharing.springbootapp"
version = "1.0"

java {
	sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
	mavenCentral()
}

val jagoCommonVersion = "1.21.40"

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.jago.common:app:${jagoCommonVersion}")
	implementation("com.jago.common:kafka:${jagoCommonVersion}")

	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")

	testImplementation("com.jago.common:test:${jagoCommonVersion}")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<Test> {
	useJUnitPlatform()
}
