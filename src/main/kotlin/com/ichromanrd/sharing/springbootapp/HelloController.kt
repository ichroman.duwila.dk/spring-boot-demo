package com.ichromanrd.sharing.springbootapp

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloController {
    @GetMapping
    fun hello() = "Hello DK!"

    @GetMapping("/{name}")
    fun helloWithName(@PathVariable name: String) = "hello $name"

    @PostMapping
    fun postSomething(@RequestBody payload: Map<String, Any>): Any {
        return payload
    }
}