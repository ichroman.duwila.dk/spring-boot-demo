# spring-boot-demo

## Summary

- Kotlin is quite similar to Typescript, Kotlin is a derivative language of Java, built on top of JVM, statically-typed and an object-oriented language.
- Gradle is quite similar to NPM, works as a dependency manager, build and automation tool.
- Spring Boot is a tool powered by Spring Framework to speed up developing services, equipped with powerful libraries from Spring Project.
- DK Gradle Distribution is a standard Gradle binary to help us create a new project with default common configuration.
- DK Kotlin Libraries is a set of libraries collection commonly used by various DK services such as Kafka, Vault, or K8s.

## References

- [Spring Boot Starter](https://start.spring.io/)
- [Spring Boot Docs](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/)
- [Kotlin Docs](https://kotlinlang.org/docs/home.html)
- [Gradle Docs](https://docs.gradle.org/current/userguide/userguide.html)
- [DK Gradle Distribution](https://gitlab.com/dk-digital-bank/gradle-distribution/)
- [DK Kotlin Libraries](https://gitlab.com/dk-digital-bank/modules/kotlin-libraries/)
- [Maven Public Registry](https://mvnrepository.com/)

## DK services using Kotlin & Spring Boot

- [ms-report](https://gitlab.com/dk-digital-bank/services/ms-report)
- [ms-inquiry](https://gitlab.com/dk-digital-bank/services/ms-inquiry/)
- [ms-pocket](https://gitlab.com/dk-digital-bank/services/ms-pocket/)
- [ms-partner-customer](https://gitlab.com/dk-digital-bank/services/ms-partner-customer/)
- [ms-kyc](https://gitlab.com/dk-digital-bank/services/ms-kyc/)
- etc

## Cheatsheet

- To run Spring Boot application: `./gradlew bootRun`
- To run Spring Boot application with specific environment: `./gradlew bootRun --args='--spring.profiles.active={environment_name}'`
- To build Gradle project: `./gradlew build`